// Dependency:
// parseUri 1.2.2
// (c) Steven Levithan <stevenlevithan.com>
// MIT License

function parseUri (str) {
    var o   = parseUri.options,
        m   = o.parser[o.strictMode ? "strict" : "loose"].exec(str),
        uri = {},
        i   = 14;

    while (i--) uri[o.key[i]] = m[i] || "";

    uri[o.q.name] = {};
    uri[o.key[12]].replace(o.q.parser, function ($0, $1, $2) {
        if ($1) uri[o.q.name][$1] = $2;
    });

    return uri;
};

parseUri.options = {
    strictMode: true,
    key: ["source","protocol","authority","userInfo","user","password","host","port","relative","path","directory","file","query","anchor"],
    q:   {
        name:   "queryKey",
        parser: /(?:^|&)([^&=]*)=?([^&]*)/g
    },
    parser: {
        strict: /^(?:([^:\/?#]+):)?(?:\/\/((?:(([^:@]*)(?::([^:@]*))?)?@)?([^:\/?#]*)(?::(\d*))?))?((((?:[^?#\/]*\/)*)([^?#]*))(?:\?([^#]*))?(?:#(.*))?)/,
        loose:  /^(?:(?![^:@]+:[^:@\/]*@)([^:\/?#.]+):)?(?:\/\/)?((?:(([^:@]*)(?::([^:@]*))?)?@)?([^:\/?#]*)(?::(\d*))?)(((\/(?:[^?#](?![^?#\/]*\.[^?#\/.]+(?:[?#]|$)))*\/?)?([^?#\/]*))(?:\?([^#]*))?(?:#(.*))?)/
    }
};



var webpage = require('webpage'),
    system = require('system'),
    fs = require('fs'),
    address, output, size;

var pageTitle;
var addres;
var imageUrl;
var retrieveRemotely = false;

var sourcePage = webpage.create(),
    targetPage = webpage.create();

var renderChrome = function() {
    /**
    * This renders the browser around the downloaded screenshot
    */
    targetPage.open(phantom.libraryPath + "/index.html", function(status) {
        console.log("Adding browser chrome");
        if (status === 'success') {
            window.setTimeout(function () {
                var context = {
                    address: address,
                    title: pageTitle,
                    imageUrl: imageUrl
                }
                targetPage.evaluate(function(context) {
                    var e = document.getElementById('windowtitle');
                    e.innerText = context.title;
                    if (!context.title) { e.innerText = "file://" + context.imageUrl; }
                    e = document.getElementById('address');
                    e.innerText = context.address;
                    e = document.getElementById('tabtitle');
                    e.innerText = context.title;
                    if (context.imageUrl) {
                        e = document.getElementById('source-screenshot');
                        e.src = "file://" + context.imageUrl;
                    }
                    setSize();
                }, context);
                window.setTimeout(function () {
                    targetPage.render(system.args[2]);
                    phantom.exit();
                }, 200);
            }, 200);
        } else {
            console.log('Unable to load the targetPage address!');
            phantom.exit();
        }
    });
};


if (system.args.length < 2 || system.args.length > 4) {
    console.log(absoluteScriptPath)
    console.log('Usage: phantomjs shot.js URL filename [paperwidth*paperheight|paperformat] [zoom]');
    console.log('  paper (pdf output) examples: "5in*7.5in", "10cm*20cm", "A4", "Letter"');
    console.log('  image (png/jpg output) examples: "1920px" entire page, window width 1920px');
    console.log('                                   "800px*600px" window, clipped to 800x600');
    phantom.exit(1);
} else {
    address = system.args[1];
    imageUrl = null;
    if (parseUri(address).host) {
        console.log("recognised as website");
        imageUrl = phantom.libraryPath + '/screenshot_without_chrome.png';
        retrieveRemotely = true;
    } else {
        console.log("recognised as local file");
        imageUrl = fs.absolute(address);
    }
    output = imageUrl;
    sourcePage.viewportSize = { width: 600, height: 600 };

    // Configure media options
    if (system.args.length > 3 && system.args[2].substr(-4) === ".pdf") {
        size = system.args[3].split('*');
        sourcePage.paperSize = size.length === 2 ? { width: size[0], height: size[1], margin: '0px' }
                                           : { format: system.args[3], orientation: 'portrait', margin: '1cm' };
    } else if (system.args.length > 3 && system.args[3].substr(-2) === "px") {
        size = system.args[3].split('*');
        if (size.length === 2) {
            pageWidth = parseInt(size[0], 10);
            pageHeight = parseInt(size[1], 10);
            sourcePage.viewportSize = { width: pageWidth, height: pageHeight };
            sourcePage.clipRect = { top: 0, left: 0, width: pageWidth, height: pageHeight };
        } else {
            console.log("size:", system.args[3]);
            pageWidth = parseInt(system.args[3], 10);
            pageHeight = parseInt(pageWidth * 3/4, 10); // it's as good an assumption as any
            console.log ("pageHeight:",pageHeight);
            sourcePage.viewportSize = { width: pageWidth, height: pageHeight };
        }
    }
    if (system.args.length > 4) {
        sourcePage.zoomFactor = system.args[4];
    }

    if (retrieveRemotely) {
        console.log("Generating screenshot");
        sourcePage.open(address, function (status) {
            if (status !== 'success') {
                console.log('Unable to load the sourcePage address' + address + '!');
                phantom.exit();
            } else {
                window.setTimeout(function () {
                    sourcePage.render(output);
                    pageTitle = sourcePage.evaluate(function(course) {
                        return document.title;
                    });
                    renderChrome();
                }, 200);
            }
        });
    } else {
        renderChrome();
    }
}

